const {response} = require('express')
const express = require('express');
const path = require('path');
const fs = require('fs');
const url = require('url');

const PORT = 8080;
const app = express();
const dir = path.join(__dirname, 'api', 'files');
const filenamePattern = new RegExp(/^[A-z1-9.]+\.(txt|log|json|xml|js|yaml)$/);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/api/files', (req, res) => {
    try {
      fs.readdir(dir, (err, files) => {
        if (err) {
          res.status(400).json({
            "message": "Client error"
          });
        }
        res.status(200).json({
          "message": "Success",
          files
        })
      })
    } catch (err) {
      res.status(500).json({
        "message": "Server error"
      })
    }
  })

app.post('/api/files', (req, res) => {
    if (!fs.existsSync('api')) {
      fs.mkdirSync('api')
    }
    if (!fs.existsSync('api/files')) {
      fs.mkdirSync('api/files')
    }

    if (typeof req.body.content === 'undefined') {
      res.status(400).json({
        "message": "Please specify 'content' parameter"
      })
    } else if (!filenamePattern.test(req.body.filename)) {
      res.status(400).json({
        "message": "Please write 'filename' with one of the extensions: log, txt, json, yaml, xml, js"
      });
    } else {
      let filePath = path.join(dir, req.body.filename);
      fs.writeFile(filePath, req.body.content, err => {
        if (err) {
          res.status(400).json({
            "message": "Client error"
          });
        }
        res.status(200).json({
          "message": "File created successfully",
          "status": 200
        });
      })
    }
  })

app.get('/api/files/:filename', (req, res) => {
  fs.stat(path.join(__dirname, req.url), (err, stat) => {
    if (err) {
      res.status(400).json({
        "message": `No file with ${path.basename(req.url)} filename found`
      });
    }

    fs.readFile(path.join(__dirname, req.url), 'utf8', (err, data) => {
      if (err) {
        res.status(400).json({
          "message": "Client error"
        });
      }
      res.status(200).json({
        "message": "Success",
        "filename": path.basename(req.url),
        "content": data,
        "extension": path.extname(req.url).split('.').join(''),
        "uploadedDate": stat.birthtime
      })
    })
  })
})

app.listen(PORT);